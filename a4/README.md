# LIS 4368 Advanced Web Applications - Assignment 4

## Spencer Finkel

>### Assignment 4 Requirements:
>
>1. Server-side validation
>2. JavaBeans
>2. Java Standard Tag Library (JSTL)



#### Assignment Screenshots:

*Screenshot of server-side validation in action:
![A4 home page ](img/page.png) ![A4 Validated ](img/valid.png)


*Bitbucket Link ~*
https://bitbucket.org/MushinMiscellanea/lis4368/src/master/
