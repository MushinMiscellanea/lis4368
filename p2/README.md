# LIS 4368 Advanced Web Applications - Project 2 

## Spencer Finkel

### Project 2 Requirements:
>1. client & server-side validation
>2. Create, Read, Update, Delete (CRUD) functionality
>2. MVC framework
>3. Creation of page to modify/delete data
>4. Chapter questions (Chs. 16, 17)




* Knowledge and success


#### Assignment Screenshots:


![Valid](img/insert.png  "Valid user form entry (customerform.jsp)")![Passed Validation (thanks.jsp)](img/validation.png)
![Passed Validation (thanks.jsp)](img/addition.png "Display Data (customers.jsp)")
![Passed Validation (thanks.jsp)](img/modification.png "Modify Form(modify.jsp)")
![Passed Validation (thanks.jsp)](img/after_mod.png "Modified Data(customers.jsp)")
![Passed Validation (thanks.jsp)](img/delete.png "Delete Warning(customers.jsp)")
![Passed Validation (thanks.jsp)](img/sql.png "Associated Database Changes(Select, Insert, Update, Delete)")


*Bitbucket Link ~*
https://bitbucket.org/MushinMiscellanea/lis4368/src/master/
