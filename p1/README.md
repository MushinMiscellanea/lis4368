# LIS 4368 Advanced Web Applications - Project 1

## Spencer Finkel

>### Project 1 Requirements:
>
>1. Basi client-side validation
>2. Use Regular Expression to validate information
>2. Uniform pages throughout web application
>3. MVC model
>4. Chapter questions (Chs. 9, 10)




* Knowledge and success


#### Assignment Screenshots:

LIS4368 portal:
![Portal](img/portal.png)

http://localhost:9999/lis4368/p1 wrong validation:
![Failed Validation](img/wrong.png)


http://localhost:9999/lis4368/p1 passed validation:
![Correct Validation](img/validated.png)



*Bitbucket Link ~*
https://bitbucket.org/MushinMiscellanea/lis4368/src/master/
