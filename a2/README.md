<!-- >> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
> -->

# LIS 4369 Advanced Web Applications - Assignment 1

## Spencer Finkel

>### Assignment 1 Requirements:
>
>1. Access mysql through Ampps
>2. Write a database Servlet deploying Servlet using @WebServlet
>2. Develop and deploy a web application
>3. HTML and Java code
>4. Chapter questions (Chs. 5, 6)

#### README.md file should include the following items:

* Screenshots of `http://localhost:9999/hello` and accompanying pages
* Knowledge and success


#### Assignment Screenshots:

*Screenshot of index.html in home directory running http://localhost:9999/hello*:
![Running http://localhost:9999/hello ](jpg/hello.png)


*Screenshot of http://localhost:9999/hello/sayhello*:
![http://localhost:9999/hello/sayhello](jpg/sayhello.png)


*Screenshot of http://localhost:9999/hello/querybook.html*:
![http://localhost:9999/hello/querybook.html](jpg/querybook.png)


*Screenshot of http://localhost:9999/hello/sayhi*:
![http://localhost:9999/hello/sayhi](jpg/hiagain.png)


*Screenshot of http://localhost:9999/hello/querybook.html*:
![http://localhost:9999/hello/querybook.html](jpg/query2.png)


*Screenshot of query result*:
![result](jpg/queryresult.png)

*Bitbucket Link ~*
https://bitbucket.org/MushinMiscellanea/lis4368/src/master/
