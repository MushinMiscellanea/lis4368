<!-- **NOTE:** This README.md file should be placed at the **root of each of your main directory.** -->

# LIS 4368 - Advanced Web Applications

## Spencer Finkel


### LIS 4368 Requirements:

### *Assignments:*

1. [A1 README.md](https://bitbucket.org/MushinMiscellanea/lis4368/src/master/a1/ "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Get web server up and running
    - familiarization with directory structure (Servlets/JSP)
    - Provide screenshots of Installations
    - Create remote repository (Bitbucket)
    - Reideration of Git commands

2. [A2 README.md](https://bitbucket.org/MushinMiscellanea/lis4368/src/master/a2/ "My A2 README.md file")
    - Access mysql through Ampps
    - Write a database Servlet deploying Servlet using @WebServlet
    - Develop and deploy a web application
    - HTML and Java code

3. [A3 README.md](https://bitbucket.org/MushinMiscellanea/lis4368/src/master/a3/ "My A3 README.md file")
    - Entity Relationship Diagram
    - Fill tables
    - Questions

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Server-side validation
    - JavaBeans
    - Java Standard Tag Library (JSTL)

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Utilize CRUD functions for persistant storage
    - show MySQL table updates
    - complete validation
### *Projects*

1. [P1 README.md](https://bitbucket.org/MushinMiscellanea/lis4368/src/master/p1/ "My P1 README.md file")
    -  Basic client-side validation
    - Regex for validation
    - Webapp uniformity
    - MVC model
    - Chapter questions

2. [P2 README.md](p2/README.md "My P2 README.md file")
    - client & server-side validation
    - Create, Read, Update, Delete (CRUD) functionality
    - MVC framework
    - Creation of page to modify/delete data
    - Chapter questions (Chs. 16, 17)