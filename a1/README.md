<!-- >> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
> -->

# LIS 4369 Advanced Web Applications - Assignment 1

## Spencer Finkel

>### Assignment 1 Requirements:
>
>1. Setup remote and local repositories (BitBucket)
>2. Installation of JDK, Ampps, and Apache-tomcat
>3. Java/Servlet/JSP development
>4. Chapter questions (Chs. 1-4)

#### README.md file should include the following items:

* Screenshot of `Javac HelloWorld.java`
* Screenshot of running `http://localhost:9999/`
* Git commands
* Knowledge and success


#### Git commands w/short descriptions:
 1. `$ git add` stages files to be commited and pushed
 2. `$ git commit` saves changes to local repository
 3. `$ git push` pushes commited files to remote repo
 4. `$ git status` displays files in staging area
 5. `$ git clone` clones remote to local or local to remote
 6. `$ git remote -v` lists all working remote repositories
 7. `$ git brach` shows all repo branches including working branch

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost:9999/lis4398/a1*:

![Running http://localhost:9999/lis4368/a1/ ](img/a1_web.png)
![AMPPS Installation ](img/ampps.png)


*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/javaHello.png)

*Bitbucket Link ~*
https://bitbucket.org/MushinMiscellanea/lis4368/src/master/
